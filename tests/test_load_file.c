#include "src/util.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static const char TEST_FILE_PATH[] = "textures/gl2-hello-1.png";
static const size_t TEST_FILE_PATH_LEN = 25;

static int test_load_file(const char* root_dir) {
    // Combine root_dir with test_file
    size_t root_dir_len = strlen(root_dir);
    size_t path_length = root_dir_len + TEST_FILE_PATH_LEN + 1; // Added 1 for path sep
    char* full_path = (char*)(calloc(path_length+1, sizeof(char)));
    strncpy(full_path, root_dir, root_dir_len);
    strncat(full_path, "/", 1);
    strncat(full_path, TEST_FILE_PATH, TEST_FILE_PATH_LEN);

    // Load file
    int data_size;
    uint8_t* data = load_file(full_path, &data_size);
    free(full_path);
    if (data == NULL) {
        return 1;
    }

    free(data);
    return 0;
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "test_load_file takes exactly 1 argument: root_dir\n");
        return 1;
    }
    return test_load_file(argv[1]);
}
