#include "texture.h"
#include <png.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

static const uint8_t BITS_PER_CHANNEL = 8;
static const int LOAD_PNG_TRANSFORMS = PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING;

static uint8_t *load_png(const char* filename, size_t *width, size_t *height, size_t *data_size) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        fprintf(stderr, "[ERROR] Cannot open PNG file: '%s'\n", filename);
        return NULL;
    }
    // Check if PNG file
    uint8_t header[8];
    fread(header, sizeof(uint8_t), 8, file);
    if (png_sig_cmp(header, 0, 8)) {
        fprintf(stderr, "[ERROR] '%s' is not a PNG file\n", filename);
        return NULL;
    }

    // Set up PNG read structs
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "[ERROR] Failed to create PNG read struct\n");
        return NULL;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fprintf(stderr, "[ERROR] Failed to create PNG info struct\n");
        return NULL;
    }

    png_infop end_info = png_create_info_struct(png_ptr);
    if (end_info == NULL) {
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
        fprintf(stderr, "[ERROR] Failed to create end PNG info struct\n");
        return NULL;
    }

    // Connect file pointer to libpng
    png_init_io(png_ptr, file);
    png_set_sig_bytes(png_ptr, 8);

    // Set sensible dimension limits
    GLint max_tex_size;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_tex_size);
    if (max_tex_size > 4092) {
        max_tex_size = 4092;
    }
    png_set_user_limits(png_ptr, max_tex_size, max_tex_size);
    
    /*
        It would be much smarter/efficient to load the image row-by-row into a flat byte array to begin with.
        The method below is the dumb/fast/easy way, so I can just have a working example. Having 2 copies of an image
        in memory for half of a second is a small price to pay for coding time.
    */

    // Begin read
    png_read_png(png_ptr, info_ptr, LOAD_PNG_TRANSFORMS, NULL);
    uint8_t** row_pointers = png_get_rows(png_ptr, info_ptr);

    // Get required image info
    *width = png_get_image_width(png_ptr, info_ptr);
    *height = png_get_image_height(png_ptr, info_ptr);
    const uint8_t channel_count = png_get_channels(png_ptr, info_ptr);
    const uint8_t bytes_per_pixel = (BITS_PER_CHANNEL / 8) * channel_count;
    const size_t bytes_per_row = bytes_per_pixel * *width;

    // Flatten row_pointers
    *data_size = bytes_per_row * *height;
    uint8_t* pixel_data = (uint8_t*)(malloc(*data_size));
    // OpenGL reads the rows in reverse order, so the copy logic below flips the vertical axis.
    size_t count = 0;
    for (int row = (int)(*height-1); row >= 0; row--) {
        size_t offset = bytes_per_row * count;
        memcpy(pixel_data+offset, row_pointers[row], bytes_per_row);
        count++;
    }

    // Free libpng data
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

    return pixel_data;
}

/**
 * Creates a new OpenGL texture from a PNG image file.
 * 
 * \param filename relative or absolute path to the image file
 * \return GLuint object handle for the new texture
 */
GLuint create_texture_from_png(const char* filename) {
    GLuint texture = 0;
    size_t width, height, data_size;
    uint8_t *pixels = load_png(filename, &width, &height, &data_size);
    if (pixels == NULL) {
        return 0;
    }

    // Create GL texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexImage2D(
        GL_TEXTURE_2D, 0,
        GL_RGB8,
        width, height, 0,
        GL_RGB, GL_UNSIGNED_BYTE,
        pixels
    );

    free(pixels);
    return texture;
}
