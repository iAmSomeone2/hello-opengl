#include "util.h"

#include <stdlib.h>

/**
 * Load an arbitrary file into memory.
 * 
 * Caller must free the buffer returned by this function if the operation was successful.
 * 
 * \param filename path to the file
 * \param size number of bytes read from file into the buffer
 * 
 * \return pointer to buffer containing the loaded file data
 */
uint8_t* load_file(const char* filename, int *size) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        fprintf(stderr, "[ERROR] Unable to open '%s' for reading.\n", filename);
        return NULL;
    }
    // Get file size
    fseek(file, 0, SEEK_END);
    *size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Create buffer and load file
    uint8_t* buffer = (uint8_t*)(malloc(*size));
    *size = fread(buffer, sizeof(uint8_t), *size, file);
    fclose(file);

    return buffer;
}

/**
 * Load a TGA image file into memory.
 * 
 * Caller must free the buffer returned by this function if the operation was successful.
 * 
 * \param filename path to TGA file
 * \param width width in pixels as read from the file header
 * \param height height in pixels as read from the file header
 * 
 * \return pointer to buffer containing the decoded pixel data
 */
uint8_t* read_tga(const char *filename, uint16_t *width, uint16_t *height) {
    struct tga_header {
        uint8_t id_length;
        uint8_t color_map_type;
        uint8_t data_type_code;
        uint8_t color_map_origin[2];
        uint8_t color_map_length[2];
        uint8_t color_map_depth;
        uint8_t x_origin[2];
        uint8_t y_origin[2];
        uint8_t width[2];
        uint8_t height[2];
        uint8_t bits_per_pixel;
        uint8_t image_descriptor;
    } header;

    return NULL;
}
