#pragma once

#include <GL/glut.h>

typedef struct GResources GResources;
struct GResources {
    GLuint vertex_buffer;
    GLuint element_buffer;

    GLuint textures[2];

    // Shaders and rendering data
    GLuint vertex_shader, fragment_shader, program;

    struct {
        GLint fade_factor;
        GLint textures[2];
    } uniforms;

    struct {
        GLint position;
    } attributes;

    GLfloat fade_factor;
};
