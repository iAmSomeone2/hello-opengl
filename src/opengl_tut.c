#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <GL/glew.h>

#include "texture.h"
#include "util.h"
#include "gl_resources.h"

/*
    Global GResources

    TODO: Figure out better option than a global struct.
*/
static GResources g_resources;

static const GLfloat g_vertex_buffer_data[] = {
    -1.0f, -1.0f,
     1.0f, -1.0f,
    -1.0f,  1.0f,
     1.0f,  1.0f 
};
static const GLushort g_element_buffer_data[] = { 0, 1, 2, 3 };

static GLuint make_buffer(GLenum target, const void* buffer_data, GLsizei buffer_size) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(target, buffer);
    glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);

    return buffer;
}

static GLuint make_shader(GLenum type, const char *filename) {
    GLint length;
    GLchar *source = (GLchar*)(load_file(filename, &length));
    GLuint shader;
    GLint shader_ok;

    if (source == NULL) {
        fprintf(stderr, "[ERROR] Failed to load shader from file: '%s'\n", filename);
        return 0;
    }

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    free(source);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if(!shader_ok) {
        fprintf(stderr, "[ERROR] Failed to compile '%s':\n", filename);
        // show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;

    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "[ERROR] Failed to link shader program:\n");
        // show_info_log(program, glGetShaderiv, glGetShaderInfoLog);
        glDeleteProgram(program);
        return 0;
    }

    return program;
}

static bool make_resources() {
    // Set up buffers
    g_resources.vertex_buffer = make_buffer(
        GL_ARRAY_BUFFER,
        g_vertex_buffer_data,
        sizeof(g_vertex_buffer_data)
    );
    g_resources.element_buffer = make_buffer(
        GL_ELEMENT_ARRAY_BUFFER,
        g_element_buffer_data,
        sizeof(g_element_buffer_data)
    );

    // Set up textures and shaders
    g_resources.textures[0] = create_texture_from_png("textures/gl2-hello-1.png");
    g_resources.textures[1] = create_texture_from_png("textures/gl2-hello-2.png");
    if (g_resources.textures[0] == 0 || g_resources.textures[1] == 0) {
        return false;
    }

    g_resources.vertex_shader = make_shader(
        GL_VERTEX_SHADER,
        "shaders/hello.vs"
    );
    if (g_resources.vertex_shader == 0) {
        return false;
    }

    g_resources.fragment_shader = make_shader(
        GL_FRAGMENT_SHADER,
        "shaders/hello.fs"
    );
    if (g_resources.fragment_shader == 0) {
        return false;
    }

    g_resources.program = make_program(
        g_resources.vertex_shader,
        g_resources.fragment_shader
    );
    if (g_resources.program == 0) {
        return false;
    }

    g_resources.uniforms.fade_factor
        = glGetUniformLocation(g_resources.program, "fade_factor");
    g_resources.uniforms.textures[0]
        = glGetUniformLocation(g_resources.program, "textures[0]");
    g_resources.uniforms.textures[1]
        = glGetUniformLocation(g_resources.program, "textures[1]");

    g_resources.attributes.position
        = glGetAttribLocation(g_resources.program, "position");

    return true;
}

static void render() {
    glUseProgram(g_resources.program);
    glUniform1f(g_resources.uniforms.fade_factor, g_resources.fade_factor);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_resources.textures[0]);
    glUniform1i(g_resources.uniforms.textures[0], 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, g_resources.textures[1]);
    glUniform1i(g_resources.uniforms.textures[1], 1);

    // Set up vertex array
    glBindBuffer(GL_ARRAY_BUFFER, g_resources.vertex_buffer);
    glVertexAttribPointer(
        g_resources.attributes.position,    /* attribute */
        2,                                  /* size */
        GL_FLOAT,                           /* type */
        GL_FALSE,                           /* normalized? */
        sizeof(GLfloat)*2,                  /* stride */
        (void*)0                            /* array buffer offset */
    );
    glEnableVertexAttribArray(g_resources.attributes.position);

    // Submit rendering job
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_resources.element_buffer);
    glDrawElements(
        GL_TRIANGLE_STRIP,  /* mode */
        4,                  /* count */
        GL_UNSIGNED_SHORT,  /* types */
        (void*)0            /* element array buffer offset */
    );
    
    // Clean up vertex attribute array
    glDisableVertexAttribArray(g_resources.attributes.position);

    // glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // glClear(GL_COLOR_BUFFER_BIT);
    glutSwapBuffers();
}

static void update_fade_factor() {
    const int milliseconds = glutGet(GLUT_ELAPSED_TIME);
    g_resources.fade_factor = sinf((float)milliseconds * 0.001f) * 0.5f + 0.5f;
    glutPostRedisplay();
}

int main(int argc, char* argv[]) {
    // GLUT init
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(400, 300);
    glutCreateWindow("Hello World");
    glutDisplayFunc(&render);
    glutIdleFunc(&update_fade_factor);

    // GLEW init
    GLenum err = glewInit();
    if(err != GLEW_OK) {
        fprintf(stderr, "[Error] %s\n", glewGetErrorString(err));
    }
    if (!GLEW_VERSION_4_0) {
        fprintf(stderr, "[Error] OpenGL 4 not available\n");
        return EXIT_FAILURE;
    }

    // Load resources
    if (!make_resources()) {
        fprintf(stderr, "[Error] Failed to load resources\n");
        return EXIT_FAILURE;
    }

    glutMainLoop();

    return EXIT_SUCCESS;
}
