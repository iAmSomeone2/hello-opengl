#pragma once

#include <stdint.h>
#include <stdio.h>

/**
 * Load an arbitrary file into memory.
 * 
 * Caller must free the buffer returned by this function if the operation was successful.
 * 
 * \param filename path to the file
 * \param size number of bytes read from file into the buffer
 * 
 * \return pointer to buffer containing the loaded file data
 */
uint8_t* load_file(const char* filename, int *size);

/**
 * Load a TGA image file into memory.
 * 
 * Caller must free the buffer returned by this function if the operation was successful.
 * 
 * \param filename path to TGA file
 * \param width width in pixels as read from the file header
 * \param height height in pixels as read from the file header
 * 
 * \return pointer to buffer containing the decoded pixel data
 */
uint8_t* read_tga(const char *filename, uint16_t *width, uint16_t *height);
