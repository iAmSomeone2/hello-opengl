#pragma once

#include <stdint.h>
#include <GL/glew.h>
#include <GL/glut.h>

/**
 * Creates a new OpenGL texture from a PNG image file.
 * 
 * \param filename relative or absolute path to the image file
 * \return GLuint object handle for the new texture
 */
GLuint create_texture_from_png(const char* filename);
